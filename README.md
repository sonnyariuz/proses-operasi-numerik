#include <iostream>
#include <iomanip>
#include <stdlib.h>

using namespace std;

int main()
{
    system("clear");

    bool TF = false;
    int i = 0, j = 0;
    float x = 0.0, y = 0.0;
    

    // proses operasi numerik
    i = 1;
    j = 2;
    cout << "operasi numerik pada tipe data integer" << endl;
    cout << "---------------------------" << endl;
    cout << "| operasi | hasil operasi |" << endl;
    cout << "---------------------------" << endl;
    cout << "| " << i << " + " << j << "   | " << setw(8) << (i + j) << setw(7) << "|" << endl;
    cout << "| " << i << " - " << j << "   | " << setw(8) << (i - j) << setw(7) << "|" << endl;
    cout << "| " << i << " * " << j << "   | " << setw(8) << (i * j) << setw(7) << "|" << endl;
    cout << "| " << i << " div " << j << " | " << setw(8) << (i / j) << setw(7) << "|" << endl;
    cout << "| " << i << " mod " << j << " | " << setw(8) << (i % j) << setw(7) << "|" << endl;
    cout << "---------------------------" << endl;

   
    return 0;
}
